struct Block{T} end
Block(T) = Block{T}()
get_block(::Block{T}) where T = T

abstract type AbstractBlock end

include("eval/code.jl")
include("eval/command.jl")
include("eval/header.jl")
include("eval/list.jl")
include("eval/math.jl")
include("eval/paragraph.jl")
include("eval/table.jl")
