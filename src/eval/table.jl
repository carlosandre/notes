struct TableCell
	contents::Vector{FormattedString}
	rowspan::Integer
	colspan::Integer
end

struct TableLine
	cells::Vector{TableCell}
end

struct Table <: AbstractBlock
	header::Vector{TableLine}
	lines::Vector{TableLine}
end

function TableLine(str::AbstractString)::TableLine
	strcells = split(str, '|')
	result = TableCell[]

	let next = iterate(strcells)
		while next !== nothing
			(cell, state) = next

			colspan = 1
			pnext = iterate(strcells, state)
			while pnext !== nothing
				(pcell, pstate) = pnext
				if strip(pcell) == "<"
					colspan += 1
				else
					break
				end
				pnext = iterate(strcells, pstate)
			end
			state += (colspan - 1)

			if cell == "<" # if `<` was not captured in the last loop, it was used at the start of a line
				error("""Error while processing table!
					  Colspan operator (`<`) used incorrectly on the following line:

					  $str""")
			end

			push!(result, TableCell(text_format(cell |> strip), 0, colspan))
			
			next = iterate(strcells, state)
		end
	end
	
	return TableLine(result)
end

function evaluate(block::RawTable)::Table
	strlines = split(strip(block.contents), '\n')
	result = Table(TableLine[], TableLine[])
	
	for line in strlines
		if !startswith(line, '|') break end
		if isempty(line[2:end]) continue end
		push!(result.header, TableLine(line[2:end]))
	end

	for line in strlines[length(result.header)+1:end]
		if isempty(line) || strip(line) == "|" continue end
		push!(result.lines, TableLine(line))
	end

	return result
end
