struct HTMLOutput end
function render_cmd(data::Command, ::Type{Val{CmdImage}}, ::HTMLOutput)::String
	mime = data.contents[:filemimetype]
	blob = data.contents[:blob]
	return "<img src=\"data:$mime;base64,$blob\" class=\"cmdimage\"/>"
end

function render_cmd(data::Command, ::Type{Val{CmdNothing}}, ::HTMLOutput)::String
	return ""
end
