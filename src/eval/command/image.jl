using Base64

const mimetype_extensions = Dict{String, String}(
	"png" => "image/png",
	"jpg" => "image/jpeg",
	"gif" => "image/gif",
)

struct UnsupportedHeredocException <: Exception end

struct WrongNumberOfArgumentsException <: Exception
	expected::Integer
	got::Integer
end

function cmd_image(block::RawCommand)

#TODO options

	if !isempty(block.heredocs)
		throw(UnsupportedHeredocException())
	elseif length(block.contents) != 1
		throw(WrongNumberOfArgumentsException(1, length(block.contents)))
	end

	imgfile = block.contents[1]
	mimetype = mimetype_extensions[split(imgfile, ".")[end]]

	blob = base64encode(open(imgfile, "r"))
	contents = Dict{Symbol, Any}(:filemimetype => mimetype, :blob => blob)

	return Command(contents, Val{CmdImage})
end
