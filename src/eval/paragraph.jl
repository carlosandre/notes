struct Paragraph <: AbstractBlock
	contents::Vector{FormattedString}
end

function evaluate(block::RawParagraph)
	return Paragraph(filter(x -> x.contents != "", text_format(block.contents)))
end
