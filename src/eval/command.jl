struct Command <: AbstractBlock
	contents::Dict{Symbol, Any}
	outformat
end

struct CommandNotFoundException <: Exception
	command::String
end

@enum CmdOutputFormats CmdImage CmdTable CmdText CmdNothing

include("command/image.jl")
include("command/toc.jl")

include("command/out_html.jl")

const function_aliases = Dict{String, Function}(
	"img"   => cmd_image,
	"image" => cmd_image,
)

function evaluate(block::RawCommand)
	command_name = popfirst!(block.contents)[2:end]
	if command_name in keys(function_aliases)
		return function_aliases[command_name](block)
	else
		throw(CommandNotFoundException(command_name))
	end
end
