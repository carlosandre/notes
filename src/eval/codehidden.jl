struct Code <: AbstractBlock
	out_type::String
	output::Any
end

using Plots

function evaluate(block::RawCode)
	lines = split(block.contents, "\n")
	exprs = Expr[]
	parse_queue = ""

	for line in lines 
		parse_queue *= '\n' * line
		parse_result = Meta.parse(parse_queue)
		if parse_result.head == :incomplete
			continue
		else
			push!(exprs, parse_result)
			parse_queue = ""
		end
	end

	for e in exprs[1:end-1]
		Core.eval(Main, e)
	end

	# only the last expression's output is eligible for rendering.
	retval = Core.eval(Main, exprs[end])

	if rstrip(block.contents)[end] == ';'
		return Code("none", nothing)
	elseif retval isa Plots.Plot{Plots.PlotlyJSBackend}
		return Code("plot", retval)
	else # unknown return type
		return Code("none", nothing)
	end
end
