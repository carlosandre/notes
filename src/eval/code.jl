struct Code <: AbstractBlock
	out_type::String
	output::Any
end

function evaluate(block::RawCode)
	lines = split(block.contents, "\n")
	exprs = Expr[]
	inuse = Symbol[]
	parse_queue = ""

	for line in lines 
		parse_queue *= '\n' * line
		parse_result = Meta.parse(parse_queue)
		if parse_result isa Nothing || parse_result.head == :incomplete
			continue
		else
			push!(exprs, parse_result)
			parse_queue = ""
		end
	end

	for e in exprs[1:end-1]
		if e.head == :using
			for a in e.args
				append!(inuse, a.args)
			end
		end
		Core.eval(Main, e)
	end

	for package in inuse
		@info("Loading package $package")
		Core.eval(Notes, Expr(:using, Expr(:., package)))
	end

	# only the last expression's output is eligible for rendering.
	retval = Core.eval(Main, exprs[end])
	@info("Found Code block with return type $(typeof(retval))")

	if rstrip(block.contents)[end] == ';'
		return Code("none", nothing)
	elseif :Plots in inuse && retval isa Plots.Plot{Plots.PlotlyJSBackend}
		return Code("htmlplot", retval)
	elseif :Plots in inuse && retval isa Plots.Plot
		return Code("svgplot", retval)
	else # unknown return type
		return Code("none", nothing)
	end
end
