struct ListItem
	bullet::Char
	contents::Vector{FormattedString}
end

function ListItem(str::AbstractString)::ListItem
	return ListItem(str[1], text_format(strip(str[2:end])))
end

struct List <: AbstractBlock
	items::Vector{Union{List, ListItem}}
end

function nested_length(list::List)
	len = 0

	for element in list.items
		if element isa List
			len += nested_length(element)
		else
			len += 1
		end
	end

	return len
end

const LIST_BULLET_CHARS = ['-', '+', '*', '_', '~']

function evaluate(block::RawList)
	function list_tree(item_strings::Vector{ListItem}, parent_bullets::Vector{Char})
		current_bullet = item_strings[1].bullet
		result = List([])
		
		let next = iterate(item_strings)
			while next !== nothing
				(item, state) = next

				if item.bullet == current_bullet
					push!(result.items, item)
				elseif item.bullet in parent_bullets
					return result
				else
					child = list_tree(item_strings[state-1:end],
									  vcat(parent_bullets, current_bullet))
					state += nested_length(child) - 1
					push!(result.items, child)
				end

				next = iterate(item_strings, state)
			end
			# after all items had been processed (when next === nothing)
			return result
		end
	end

	lines = split(block.contents, "\n")
	items = String[]
	
	for line in lines
		if any(x -> startswith(line, x), LIST_BULLET_CHARS)
			push!(items, line)
		else
			items[end] *= '\n' * line
		end
	end

	items = ListItem.(items)
	return list_tree(items, Char[])
end
