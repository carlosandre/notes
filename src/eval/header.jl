struct Header <: AbstractBlock
	contents::String
	level::Int8
	seq::Vector{Int}
	tag::String
end

HEADER_SEQ = [ 0, 0, 0, 0, 0, 0 ]
HEADER_TAG = Dict{String, Tuple{String, Array{Int, 1}}}()

function evaluate(block::RawHeader)
	global HEADER_SEQ
	global HEADER_TAG

	iter = Iterators.Stateful(block.contents)

	level    = length(takewhile!(x -> x == '#', iter))
	dropwhile!(x -> isspace(x), iter)
	contents = rstrip(takewhile!(x -> x != '#', iter))
	dropwhile!(x -> x == '#', iter)
	dropwhile!(x -> isspace(x), iter)
	tag      = rstrip(takewhile!(x -> isdiglet(x), iter))

	# remove extra spaces and '#' after the tag
	dropwhile!(x -> isspace(x), iter)
	dropwhile!(x -> x == '#', iter)
	dropwhile!(x -> isspace(x), iter)

	if !isempty(iter)
		error("Unknown error found while processing header.
       Extra characters after end of tag on character number $(block.position).
\n$(block.contents)
$(" " ^ iter.taken)^ here.")
	end

	HEADER_SEQ[level] += 1; HEADER_SEQ[level+1:end] .= 0
	
	if tag != ""
		if tag in keys(HEADER_TAG)
			error("The tag $tag was already used.")
		else
			HEADER_TAG[tag] = (contents, copy(HEADER_SEQ[1:level]))
		end
	end

	return Header(contents, level, copy(HEADER_SEQ[1:level]), tag)
end
