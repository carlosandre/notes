struct Math <: AbstractBlock
	contents::String
end

using TeXmath

function evaluate(block::RawMath)
	lines = split(block.contents, "\n")
	exprs = Expr[]
	parse_queue = ""

	for line in lines 
		parse_queue *= '\n' * line
		parse_result = Meta.parse(parse_queue)
		if parse_result.head == :incomplete
			continue
		else
			push!(exprs, parse_result)
			parse_queue = ""
		end
	end

	if length(exprs) == 1
		return Math(texmath(exprs[1]) |> strip)
	else
		strs = Math(texmath_align(exprs) |> strip)
		return strs
	end
end
