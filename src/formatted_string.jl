struct FormattedString
	contents::String
	formatting::Array{Symbol}
end

function text_format(str::AbstractString)::Vector{FormattedString}
	result = Array{FormattedString, 1}()
	text = ""
	frm = Symbol[]
	iter = Iterators.Stateful(str)

	while !isempty(iter)
		char = popfirst!(iter)

		if char == '\\'
			text *= popfirst!(iter)
		elseif char == '*'
			push!(result, FormattedString(text, copy(frm))); text = ""
			if :bold in frm
				lastformat = pop!(frm)
				if lastformat != :bold
					error("$lastformat formatting ended by `*` (bold)")
				end
			else
				push!(frm, :bold)
			end

		elseif char == '_'
			push!(result, FormattedString(text, copy(frm))); text = ""
			if :italic in frm
				lastformat = pop!(frm)
				if lastformat != :italic
					error("$lastformat formatting ended by `_` (italic)")
				end
			else
				push!(frm, :italic)
			end

		elseif char == '$'
			push!(result, FormattedString(text, copy(frm))); text = ""
			if Base.peek(iter) == '['
				popfirst!(iter) # pop the '['
				brackets = 1
				while true
					char = popfirst!(iter)
					if char == '['
						brackets += 1
					elseif char == ']'
						brackets -= 1
						if brackets < 1
							break
						end
					end

					text *= char
				end
			else
				text = takewhile!(x -> isdiglet(x), iter)
				if isempty(text)
					text *= '$'
					continue
				end
			end

			push!(result, FormattedString(texmath(Meta.parse(text)), [:math])); text = ""

		elseif char == '!'
			push!(result, FormattedString(text, copy(frm))); text = ""
			if Base.peek(iter) == '['
				popfirst!(iter) # pop the '['
				push!(result, FormattedString(takeuntil!(x -> x == ']', iter), [:command])); text = ""
			else
				text = takewhile!(x -> isdiglet(x), iter)
				if isempty(text)
					text *= '!'
					continue
				end
			end

			push!(result, FormattedString(texmath(Meta.parse(text)), [:command])); text = ""
		else
			text *= char
		end
	end

	if !isempty(text)
		if isempty(frm)
			push!(result, FormattedString(text, copy(frm)))
		else
			error("""Error while formatting block.\nFormatting class `$frm` was opened, but not closed.\n\n`\n$(text)\n`""")
		end
	end

	return result
end
