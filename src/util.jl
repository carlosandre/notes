function isdiglet(c::Char)
	return isdigit(c) || isletter(c) || c == '_' || c == '-'
end

# Iterator helper functions {{{
function dropwhile!(f, iter::Iterators.Stateful)
	while !isempty(iter)
		if f(Base.peek(iter))
			popfirst!(iter)
		else
			return
		end
	end
end

function takewhile!(f, iter::Iterators.Stateful)
	result = ""

	while !isempty(iter)
		if f(Base.peek(iter))
			result *= popfirst!(iter)
		else break end
	end

	return result
end

function takeuntil!(f, iter::Iterators.Stateful)
	result = ""

	while !isempty(iter)
		c = popfirst!(iter)
		if !f(c)
			result *= c
		else break end
	end

	return result
end

function dropuntil!(f, iter::Iterators.Stateful)
	while !isempty(iter)
		c = popfirst!(iter)
		if f(c) break end
	end
end
# }}}
