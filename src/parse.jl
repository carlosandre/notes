# Block definitions {{{

struct RawBlock{T} end
RawBlock(T) = RawBlock{T}()
get_raw_block(::RawBlock{T}) where T = T

abstract type AbstractRawBlock end

struct RawParagraph <: AbstractRawBlock
	position::Int
	contents::String
end

struct RawHeader <: AbstractRawBlock
	position::Int
	contents::String
end

struct RawMath <: AbstractRawBlock
	position::Int
	contents::String
end

struct RawCode <: AbstractRawBlock
	position::Int
	contents::String
end

struct RawList <: AbstractRawBlock
	position::Int
	contents::String
end

struct RawCommand <: AbstractRawBlock
	position::Int
	contents::Vector{String}
	heredocs::Vector{String}
end

struct RawTable <: AbstractRawBlock
	position::Int
	contents::String
end

# }}}

function parse_str(str::String) # {{{
	# reset global variables to initial state
	global HEADER_SEQ = [ 0 0 0 0 0 0 ]
	global HEADER_TAG = Dict{ String, Tuple{String, Array{Int, 1}} }()

	iter = Iterators.Stateful(str)
	next = nextblock(iter)
	if next == Nothing
		next = RawParagraph
	end
	raw_blocks = Vector{AbstractRawBlock}([
		RawCode(0, "title  = nothing"),
		RawCode(0, "author = nothing"),
# 		RawCode(0, "using Plots
# 			plotlyjs()
# 			Plots.default(
# 				background_color=RGB(.07,.07,.07),
# 				foreground_color=RGB(.86,.86,.86),
# 				framestyle=:zerolines,
# 				fillalpha=0.25,
# 				label=\"\",
# 				aspect_ratio=1
# 			)")
	])

	while true
		(result, iter, next) = parse_str(iter, RawBlock(next))
		push!(raw_blocks, result)
		if next == Nothing break end
	end

	return raw_blocks
end
# }}}

function nextblock(iter::Iterators.Stateful)::DataType # {{{
	dropwhile!(x -> isspace(x) && x != '\n', iter)

	char = Base.peek(iter)
	if char == '\n'
		return RawParagraph
	elseif char == '#'
		return RawHeader
	elseif char == '!'
		return RawCommand
	elseif char == ':'
		return RawCode
	elseif char == '$'
		return RawMath
	elseif char == '|'
		return RawTable
	elseif any(x -> char == x, LIST_BULLET_CHARS)
		return RawList
	else
		return Nothing
	end
end # }}}

function parse_str(iter::Iterators.Stateful, ::RawBlock{RawParagraph}):: # {{{
	Tuple{RawParagraph, Iterators.Stateful, DataType}

	startpos = iter.taken
	result   = ""
	next     = Nothing

	while !isempty(iter)
		c = popfirst!(iter)

		if c == '\\'
			result *= '\\' * popfirst!(iter)
		elseif c == '%'
			dropwhile!(x -> x != '\n', iter)
			next = nextblock(iter)
			if next == Nothing
				continue
			else break end
		elseif c == '\n'
			next = nextblock(iter)
			if next == Nothing
				result *= c
			else break end
		else
			result *= c
		end
	end

	return (RawParagraph(startpos, result), iter, next)
end # }}}

function parse_str(iter::Iterators.Stateful, ::RawBlock{RawCommand}):: # {{{
	Tuple{RawCommand, Iterators.Stateful, DataType}

	function takearg!(iter::Iterators.Stateful)::String # {{{
		result = ""
		insidesinglequote = false
		insidedoublequote = false

		while !isempty(iter)
			c = popfirst!(iter)
			if c == '\\'
				result *= '\\' * popfirst!(iter)
			elseif c == '\''
				insidesinglequote = !insidesinglequote
				result *= c
			elseif c == '"'
				insidedoublequote = !insidedoublequote
				result *= c
			elseif isspace(c) && !insidesinglequote && !insidedoublequote
				return result
			else
				result *= c
			end
		end

		return result
	end # }}}

	startpos = iter.taken
	result   = ""
	next     = Nothing

	while !isempty(iter)
		c = popfirst!(iter)

		if c == '\\'
			result *= '\\' * popfirst!(iter)
		elseif c == '%'
			dropuntil!(x -> x != '\n', iter)
			next = nextblock(iter)
			if next == Nothing
				continue
			else break end
		elseif c == '\n'
			next = nextblock(iter)
			if next == Nothing
				next = RawParagraph
			end
			break
		else
			result *= c
		end
	end

	args         = String[]
	heredoc_keys = String[]
	heredocs     = String[]
	iter_result  = Iterators.Stateful(result)
	
	while !isempty(iter_result)
		arg = takearg!(iter_result)

		if startswith(arg, "<<") # find heredoc declarations
			push!(heredoc_keys, arg[3:end])
			push!(args, "<<")
		else
			push!(args, arg)
		end
	end

	if isempty(heredoc_keys)
		return (RawCommand(startpos, args, heredocs), iter, next)
	end

	current_heredoc     = ""
	current_heredoc_key = popfirst!(heredoc_keys)

	while true # grab heredocs
		line = takeuntil!(x -> x == '\n', iter)
		if line == current_heredoc_key
			push!(heredocs, current_heredoc)
			current_heredoc = ""
			if isempty(heredoc_keys)
				break
			else
				current_heredoc_key = popfirst!(heredoc_keys)
			end
		else
			current_heredoc *= '\n' * line
		end
	end

	return (RawCommand(startpos, args, heredocs), iter, next)
end # }}}

function parse_str(iter::Iterators.Stateful, ::RawBlock{RawList}):: # {{{
	Tuple{RawList, Iterators.Stateful, DataType}

	startpos = iter.taken
	result   = ""
	next     = Nothing

	while !isempty(iter)
		c = popfirst!(iter)

		if c == '\\'
			result *= '\\' * popfirst!(iter)
		elseif c == '%'
			dropwhile!(x -> x != '\n', iter)
			next = nextblock(iter)
			if next == Nothing
				continue
			else break end
		elseif c == '\n'
			next = nextblock(iter)
			if next == Nothing
				result *= c
			else break end
		else
			result *= c
		end
	end

	return (RawList(startpos, result), iter, next)
end # }}}

function parse_str(iter::Iterators.Stateful, ::RawBlock{RawTable}):: # {{{
	Tuple{RawTable, Iterators.Stateful, DataType}

	startpos = iter.taken
	result   = ""
	next     = Nothing

	while !isempty(iter)
		c = popfirst!(iter)

		if c == '\\'
			result *= '\\' * popfirst!(iter)
		elseif c == '%'
			dropwhile!(x -> x != '\n', iter)
			next = nextblock(iter)
			if next == Nothing
				continue
			else break end
		elseif c == '\n'
			next = nextblock(iter)
			if next == Nothing || next == RawList
				result *= c
			else break end
		else
			result *= c
		end
	end

	return (RawTable(startpos, result), iter, next)
end # }}}

function parse_str(iter::Iterators.Stateful, ::RawBlock{RawCode}):: # {{{
	Tuple{RawCode, Iterators.Stateful, DataType}

	startpos = iter.taken
	result   = ""
	next     = Nothing

	while !isempty(iter)
		c = popfirst!(iter)

		if c == '\\'
			result *= '\\' * popfirst!(iter)
		elseif c == '%'
			dropuntil!(x -> x != '\n', iter)
			next = nextblock(iter)
			if next == Nothing
				continue
			else break end
		elseif c == '\n'
			next = nextblock(iter)
			if next == Nothing
				next = RawParagraph
			end
			break
		else
			result *= c
		end
	end

	return (RawCode(startpos, strip(result)[2:end]), iter, next)
end # }}}

function parse_str(iter::Iterators.Stateful, ::RawBlock{RawHeader}):: # {{{
	Tuple{RawHeader, Iterators.Stateful, DataType}

	startpos = iter.taken
	result   = ""
	next     = Nothing

	while !isempty(iter)
		c = popfirst!(iter)

		if c == '\\'
			result *= '\\' * popfirst!(iter)
		elseif c == '%'
			dropuntil!(x -> x != '\n', iter)
			next = nextblock(iter)
			if next == Nothing
				continue
			else break end
		elseif c == '\n'
			next = nextblock(iter)
			if next == Nothing
				next = RawParagraph
			end
			break
		else
			result *= c
		end
	end

	return (RawHeader(startpos, strip(result)), iter, next)
end # }}}

function parse_str(iter::Iterators.Stateful, ::RawBlock{RawMath}):: # {{{
	Tuple{RawMath, Iterators.Stateful, DataType}

	startpos = iter.taken
	result   = ""
	next     = Nothing

	while !isempty(iter)
		c = popfirst!(iter)

		if c == '\\'
			result *= '\\' * popfirst!(iter)
		elseif c == '%'
			dropuntil!(x -> x != '\n', iter)
			next = nextblock(iter)
			if next == Nothing
				continue
			else break end
		elseif c == '\n'
			next = nextblock(iter)
			if next == Nothing
				next = RawParagraph
			end
			break
		else
			result *= c
		end
	end

	return (RawMath(startpos, strip(result)[2:end]), iter, next)
end # }}}
