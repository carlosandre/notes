const HTML_CLASSES = Dict{Symbol, String}(
	:bold => "bold",
	:italic => "italic",
	:math => "math",
	:command => "command",
)

const html_escaped_characters = Dict{Char, String}(
	'<' => "&lt;",
	'>' => "&gt;",
	'&' => "&amp;",
	'\"' => "&quot;",
)

function html_escape(str::AbstractString)
	newstr = "";
	for c in str
		if c in keys(html_escaped_characters)
			newstr = newstr * html_escaped_characters[c];
		else
			newstr = newstr * c;
		end
	end
	return newstr
end

function render(blocks::Vector{AbstractBlock}, filename="/tmp/notes.html") # {{{
	file = open(filename, "w")
	println(file, """<!DOCTYPE html>
<html>
<head>
	<title>Notes.jl</title>
	<meta charset="utf-8"/>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/katex.min.css" integrity="sha384-zB1R0rpPzHqg7Kpt0Aljp8JPLqbXI3bhnPWROx27a9N0Ll6ZP/+DiW/UqRcLbRjq" crossorigin="anonymous">
	<script defer src="https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/katex.min.js" integrity="sha384-y23I5Q6l+B6vatafAwxRu/0oK/79VlbSz7Q9aiSZUvyWYIYsd+qj+o24G5ZU2zJz" crossorigin="anonymous"></script>
	<style>
		@import url('https://rsms.me/inter/inter.css');

		body {
			font-family: Inter;
			font-size: 16pt;
			max-width: 70vw;
			width: 650pt;
			margin: auto;
			background-color: #121212;
			color: #dddddd;
			padding-top: 5em;
		}

		p { font-size: 16pt; }

		h1, h2, h3, h4, h5, h6 {
			font-family: Inter;
			font-weight: bold;
			margin-left: -56pt;
		}

		h1 .headerseq,
		h2 .headerseq,
		h3 .headerseq,
		h4 .headerseq,
		h5 .headerseq,
		h6 .headerseq {
			display: inline-block;
			min-width: 42pt;
			padding-right: 14pt;
			font-weight: normal;
			font-size: 0.69em;
		}

		.headerref:before {
			content: "#";
		}

		.headertag {
			display: block;
			float: right;
			font-size: 13pt;
			font-weight: normal;
			opacity: 0.3;
		}

		.title {
			text-align: center;
			font-size: 32pt;
			margin-top: 4em;
			margin-bottom: 3em;
		}

		.author {
			text-align: center;
			margin-top: 1em;
			margin-bottom: 4em;
		}

		.cmdimage {
			display: block;
			margin: 2.5em auto;
			max-width: 100%;
		}

		iframe {
			display: block;
			margin: auto;
			width: 615px;
			border: none;
			height: 420px;
		}
		
		li {
			margin: 0.7em 0 0.7em 0.7em;
			padding-left: 1em;
		}

		.bold { font-weight: bold; }

		.italic { font-style: italic; }
		
		div.math {
			font-size: 1.21em;
			text-align: center;
			margin: 2.5em auto;
		}

		table {
			border-top: solid 2px #dddddd;
			border-bottom: solid 2px #dddddd;
			border-collapse: collapse;
			margin: 2.5em auto;
		}

		table th {
			border-bottom: solid 1px #dddddd;
			padding: 6.5pt;
		}

		table td {
			padding: 3.25pt 13pt;
			border-bottom: solid 1px #555555;
		}

		.katex {
			font-size: 1.0em;
		}

	</style>
</head>
<body>
""")

	for b in blocks
		println(file, render(b))
	end
	
	println(file, """
			<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
		<script>
			window.addEventListener('load', function () {
				var math_divs = document.getElementsByClassName('math');

				for(var i=0; i< math_divs.length; i++){
					katex.render('\\\\displaystyle ' + math_divs[i].textContent, math_divs[i], {})
				}
			})
		</script>
		</body>
		</html>""")
	close(file)
end # }}}

function render(block::Paragraph)
	result = ""

	for f in block.contents
		if isempty(f.formatting)
			result *= html_escape(f.contents)
		else
			classes = String[]
			for c in f.formatting
				push!(classes, HTML_CLASSES[c])
			end

			result *= "<span class=\"$(join(classes, " "))\">$(html_escape(f.contents))</span>"
		end
	end

	return "<p>$result</p>"
end

function render(block::Header)
	return "<h$(block.level)" * (!isempty(block.tag) ? " id=\"" * block.tag * "\"" : "") *
		"><span class=\"headerseq\">" * join(block.seq, " &middot; ") * "</span>" *
		html_escape(block.contents) *
		(block.tag == "" ? "" : "<span class=\"headertag\">#$(block.tag)</span>") *
	"</h$(block.level)>"
end

function render(block::Math)
	return "<div class=\"math\">$(block.contents |> html_escape)</div>"
end

function render(block::Command)
	return render_cmd(block, block.outformat, HTMLOutput())
end

using Base64

function render(block::Code)
	if block.out_type == "none"
		return ""
	elseif block.out_type == "htmlplot"
		r = rand(0:1000000)
		Plots.savefig(block.output, "/tmp/Notesjl-$r.html")
		blob = base64encode(open("/tmp/Notesjl-$r.html"))

		return "<iframe seamless=\"seamless\" src=\"data:text/html;base64,$blob\"></iframe>"
	elseif block.out_type == "svgplot"
		r = rand(0:1000000)
		Plots.savefig(block.output, "/tmp/Notesjl-$r.svg")
		blob = base64encode(open("/tmp/Notesjl-$r.svg"))

		return "<iframe seamless=\"seamless\" src=\"data:image/svg+xml;base64,$blob\"></iframe>"
	else
		# TODO: better error handling
		@warn("unknown type `$(block.out_type)` returned in code block!\n\n$(dump(block))")
		return "$(block.out_type): $(block |> dump)"
	end
end

function render(block::List)
	result = "<ul>\n"

	for i in block.items
		if i isa List
			result *= render(i)
		else
			result *= "<li>"
			for f in i.contents
				if isempty(f.formatting)
					result *= html_escape(f.contents)
				else
					classes = String[]
					for c in f.formatting
						push!(classes, HTML_CLASSES[c])
					end

					result *= "<span class=\"$(join(classes, " "))\">$(html_escape(f.contents))</span>"
				end
			end
			result *= "</li>\n"
		end
	end

	result *= "</ul>\n"
	
	return result
end

function render(block::Table)
	result = "<table>\n"
	
	for line in block.header
		result *= "<tr>"
		for cell in line.cells
			if cell.rowspan > 1
				error("rowspan not supported yet!")
			elseif cell.colspan > 1
				result *= "<th colspan=\"$(cell.colspan)\">"
				for f in cell.contents
					if isempty(f.formatting)
						result *= html_escape(f.contents)
					else
						classes = String[]
						for c in f.formatting
							push!(classes, HTML_CLASSES[c])
						end

						result *= "<span class=\"$(join(classes, " "))\">" *
						          html_escape(f.contents) * "</span>"
					end
				end
				result *= "</th>"
			else
				result *= "<th>"
				for f in cell.contents
					if isempty(f.formatting)
						result *= html_escape(f.contents)
					else
						classes = String[]
						for c in f.formatting
							push!(classes, HTML_CLASSES[c])
						end

						result *= "<span class=\"$(join(classes, " "))\">" *
						          html_escape(f.contents) * "</span>"
					end
				end
				result *= "</th>"
			end
		end
		result *= "</tr>\n"
	end

	for line in block.lines
		result *= "<tr>"
		for cell in line.cells
			if cell.rowspan > 1
				error("rowspan not supported yet!")
			elseif cell.colspan > 1
				result *= "<td colspan=\"$(cell.colspan)\">"
				for f in cell.contents
					if isempty(f.formatting)
						result *= html_escape(f.contents)
					else
						classes = String[]
						for c in f.formatting
							push!(classes, HTML_CLASSES[c])
						end

						result *= "<span class=\"$(join(classes, " "))\">" *
						          html_escape(f.contents) * "</span>"
					end
				end
				result *= "</td>"
			else
				result *= "<td>"
				for f in cell.contents
					if isempty(f.formatting)
						result *= html_escape(f.contents)
					else
						classes = String[]
						for c in f.formatting
							push!(classes, HTML_CLASSES[c])
						end

						result *= "<span class=\"$(join(classes, " "))\">" *
						          html_escape(f.contents) * "</span>"
					end
				end
				result *= "</td>"
			end
		end
		result *= "</tr>\n"
	end

	result *= "</table>"
	return result
end
