module Notes2

export parse_str, preprocess, join_blocks, evaluate, render

"This list is responsible for holding the contents for all files and heredocs used inside commands."
CMDFILES = String[]

"This string contains the path to the source file, if any. Empty string if reading from a string."
FILEPATH = ""

include("util.jl")
include("parse.jl")
include("join.jl")
include("formatted_string.jl")
include("eval.jl")
include("render_html.jl")

end # module
