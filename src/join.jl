function join_blocks(blocks)
	result = AbstractRawBlock[]
	iter = Iterators.Stateful(blocks)

	while !isempty(iter)
		block = popfirst!(iter)

		if block.contents == "" continue end

		if block isa RawParagraph ||
		   block isa RawHeader    ||
		   block isa RawCommand
		   	# blocks that never are merged
			push!(result, block)
			continue
		end

		merged_contents = block.contents # contents must be of type String

		while !isempty(iter)
			next = Base.peek(iter)

			if next isa typeof(block)
				merged_contents *= '\n' * popfirst!(iter).contents
			else
				break
			end
		end

		push!(result, typeof(block)(block.position, merged_contents))
	end

	return result
end
